﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace QuoteDisplay
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private Socket level1Socket;
		private AsyncCallback level1Callback;
		private byte[] level1SocketBuffer = new byte[8096];
		private bool level1NeedBeginReceive = true;
		private string level1IncompleteRecord = "";

		public delegate void UpdateDataHandler(string sMessage);
		public delegate void UpdateControlsHandler();

		public MainWindow()
		{
			InitializeComponent();

			Connect2Socket();
		}

		private void Connect2Socket()
		{
			level1Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			IPAddress ipLocalHost = IPAddress.Parse("127.0.0.1");

			int iPort = GetIQFeedPort("Level1");

			IPEndPoint ipEndLocalHost = new IPEndPoint(ipLocalHost, iPort);

			try
			{
				level1Socket.Connect(ipEndLocalHost);
				SendRequestToIQFeed("S,SET PROTOCOL,5.1\r\n");

				WaitForData("Level1");
			}
			catch(SocketException ex)
			{
				MessageBox.Show(string.Format("Not connected. Login before getting data requests\n{0}", ex.Message));
			}
		}

		private void WaitForData(string portString)
		{
			if(portString.Equals("Level1"))
			{
				if(level1Callback == null)
					level1Callback = new AsyncCallback(OnRecieve);
			}
			if(level1NeedBeginReceive)
			{
				level1NeedBeginReceive = false;
				level1Socket.BeginReceive(level1SocketBuffer, 0, level1SocketBuffer.Length, SocketFlags.None, level1Callback, portString);
			}
		}

		private void OnRecieve(IAsyncResult ar)
		{
			int iReceivedBytes = 0;
			iReceivedBytes = level1Socket.EndReceive(ar);
			level1NeedBeginReceive = true;
			string sData = Encoding.ASCII.GetString(level1SocketBuffer, 0, iReceivedBytes);

			sData = level1IncompleteRecord + sData;

			level1IncompleteRecord = "";

			string sLine = "";
			int iNewLinePos = -1;
			while(sData.Length > 0)
			{
				iNewLinePos = sData.IndexOf("\n");
				if(iNewLinePos > 0)
				{
					sLine = sData.Substring(0, iNewLinePos);
					switch(sLine[0])
					{
						case 'Q':
							ProcessUpdateMessage(sLine);
							break;
						case 'E':
                            ProcessErrorMsg(sLine);
							break;
					}
					sData = sData.Substring(sLine.Length + 1);
				}
				else
				{
					level1IncompleteRecord = sData;
					sData = "";
				}
			}

			WaitForData("Level1");
			LimitListItems();
		}

		private void LimitListItems()
		{
			try
			{
				if(!lstBox.Dispatcher.CheckAccess())
				{
					Dispatcher.Invoke(new UpdateControlsHandler(LimitListItems));
				}
				else
				{
					while(lstBox.Items.Count > 100)
					{
						lstBox.Items.RemoveAt(100);
					}
				}
				

			}
			catch(ObjectDisposedException)
			{
				
			}

		}

		private void ProcessErrorMsg(string sLine)
		{
			UpdateListBox(sLine);
		}

		private void ProcessUpdateMessage(string sLine)
		{
			UpdateListBox(sLine);
		}

		private int GetIQFeedPort(string portId)
		{
			int port2Return = 0;
			RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\DTN\\IQFeed\\Startup");
			if(key != null)
			{
				string sData = "";
				switch(portId)
				{
					case "Level1":
						sData = key.GetValue("Level1Port", "5009").ToString();
						break;
				}
				port2Return = Convert.ToInt32(sData);
			}
			return port2Return;
		}

		private void SendRequestToIQFeed(string sCommand)
		{
			byte[] szCommand = new byte[sCommand.Length];
			szCommand = Encoding.ASCII.GetBytes(sCommand);
			int iBytes2Send = szCommand.Length;
			try
			{
				int iBytesSent = level1Socket.Send(szCommand, iBytes2Send, SocketFlags.None);
				if(iBytesSent != iBytes2Send)
				{
					UpdateListBox(string.Format("Error in sending request:\r\n{0}", sCommand.TrimEnd("\r\n".ToCharArray())));
				}
				else
				{
					UpdateListBox(string.Format("Request sent sucessfully:\r\n{0}", sCommand.TrimEnd("\r\n".ToCharArray())));
				}
			}
			catch(SocketException ex)
			{
				UpdateListBox(String.Format("Socket Error Sending Request:\r\n{0}\r\n{1}", sCommand.TrimEnd("\r\n".ToCharArray()), ex.Message));
			}
		}

		private void UpdateListBox(string string2Update)
		{
			try
			{
				if(!lstBox.Dispatcher.CheckAccess())
				{
					Dispatcher.Invoke(new UpdateDataHandler(UpdateListBox), string2Update);
				}
				else
				{
					List<string> lstMessages = new List<string>(string2Update.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries));
					lstBox.BeginInit();
					lstMessages.ForEach(delegate(string sLine)
					{
						lstBox.Items.Insert(0, sLine);
					});
					LimitListItems();
					lstBox.EndInit();
				}
			}
			catch(ObjectDisposedException)
			{
				
			}
		}

		private void StartWatch_Click(object sender, RoutedEventArgs e)
		{
			SendRequestToIQFeed(string.Format("w{0}\r\n", TextField.Text));
		}

		private void RemoveWatch_Click(object sender, RoutedEventArgs e)
		{
			SendRequestToIQFeed(string.Format("r{0}\r\n", TextField.Text));
		}

		private void RemoveAllWatches_Click(object sender, RoutedEventArgs e)
		{
			 SendRequestToIQFeed("S,UNWATCH ALL\r\n");
		}
	}
}
